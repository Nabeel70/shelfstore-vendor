import { useEffect, useState, useCallback } from "react";
import { useRouter } from "next/router";
import Link from "next/link";
import MenuDrawer from "../components/menu-drawer";
import PortalDrawer from "../components/portal-drawer";
import styles from "./print.module.css";
import qrcode from "./shop";

const Print = () => {
  const router = useRouter();
  const [isMenuDrawerOpen, setMenuDrawerOpen] = useState(false);
  

  const onShareAshelfLinkClick = useCallback(() => {
    router.push("/");
  }, [router]);

  const openMenuDrawer = useCallback(() => {
    setMenuDrawerOpen(true);
  }, []);

  const closeMenuDrawer = useCallback(() => {
    setMenuDrawerOpen(false);
  }, []);

  const onSignUpLinkClick = useCallback(() => {
    router.push("/signup");
  }, [router]);

  
    const onButtonClick = useCallback(() => {
 
      let printContents = document.getElementById('printarea').innerHTML;
        let originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
       document.body.innerHTML = originalContents; 
  
    }, []);

  

  return (

    <>
      <div className={styles.printDiv}>
        <div className={styles.footerDiv}>
          <div className={styles.rentmyshelfIncDiv}>
            © 2022 rentmyshelf, Inc.
          </div>
          <div className={styles.supportDiv}>Support</div>
          <div className={styles.contactDiv}>Contact</div>
          <div className={styles.termsOfService}>Terms of Service</div>
        </div>
        <main className={styles.main}>
        <div className={styles.rectangleDiv2}>
          <div className={styles.section2PrintDiv} id="printarea">
            <div className={styles.rectangleDiv} />
            <div className={styles.rectangleDiv1} />
            <div className={styles.qrcodeDiv}>
              <img className={styles.vectorIcon} alt="" src="../vector.svg" />
            </div>
            <div className={styles.haveSomethingToSellHaveS}>
              <span className={styles.haveSomethingToContainer}>
                <span>
                  Have something to sell? Have spare space, or a free shelf?
                  Enjoy disovering local makers? See more collaborations and
                  learn how to get involved at
                </span>
                <b>{` shareashelf.com `}</b>
              </span>
            </div>
            <div className={styles.directionsScanTheQRCodeT}>
              <p className={styles.blankLineP}>
                <b className={styles.directionsB}>Directions:</b>
                <span>
                  <span>{` Scan the QR code to purchase any of the items on display and to view the profiles of both the host and vendor. `}</span>
                </span>
              </p>
              <p className={styles.blankLineP}>
                <span>
                  <b>&nbsp;</b>
                </span>
              </p>
              <p className={styles.ifYouPurchaseSomethingPle}>
                <span>
                  <b>
                    If you purchase something, please show payment confirmation
                    to the host before taking your items.
                  </b>
                </span>
              </p>
            </div>
            <b className={styles.b}>🤝</b>
            <div className={styles.frameDiv}>
              <h6 className={styles.alphaCostumesVendor}>
                <b>
                  <span>Alpha Costumes </span>
                  <span className={styles.span}>{`• `}</span>
                </b>
                <span className={styles.span}>
                  <i className={styles.vendorI}>Vendor</i>
                </span>
              </h6>
              <h6
                className={styles.alphaCostumesMakesPremiumC}
              >{`Alpha Costumes makes premium cosplay for clients across the world; we’ve been featured in numerous magazines such as Apollo and Sci-Fi. `}</h6>
            </div>
            <div className={styles.frameDiv1}>
              <h6 className={styles.alphaCostumesVendor}>
                <b>
                  <span>Checkmate </span>
                  <span className={styles.span}>{`• `}</span>
                </b>
                <span className={styles.span}>
                  <i className={styles.vendorI}>Host</i>
                </span>
              </h6>
              <p className={styles.checkmateIsASmallCafeLoca}>
                Checkmate is a small cafe located in the heart of Edinburgh’s
                Historic New Town. We specialise in teas and boardgames from
                around the world and complement these with an ever changing
                selection of cakes.
              </p>
            </div>
            <h1 className={styles.aShareAshelfCollaboration}>
              <span className={styles.haveSomethingToContainer}>
                <span className={styles.aShareSpan}>
                  <span className={styles.aSpan}>a</span>
                  <span> share</span>
                </span>
                <span className={styles.aSpan1}>A</span>
                <span className={styles.shelfCollaborationSpan}>
                  <span className={styles.shelfSpan}>{`shelf `}</span>
                  <span>collaboration</span>
                </span>
              </span>
            </h1>
          </div>
          <div className={styles.lineDiv} />
          <div className={styles.section1Div}>
            <div className={styles.section1Div1} />
            <h1 className={styles.shareAshelfH1}>
              <span className={styles.haveSomethingToContainer}>
                <span className={styles.aShareSpan}>share</span>
                <span className={styles.aSpan2}>A</span>
                <span className={styles.shelfSpan1}>shelf</span>
              </span>
            </h1>
            <button className={styles.button} onClick={onButtonClick}>
              <b className={styles.labelText} onClick={onButtonClick}>Print Foldable Sign</b>
            </button>
          </div>
          </div>
        </main>
        <header className={styles.header}>
          <div className={styles.frameDiv2}>
            <Link href="/">
              <a
                className={styles.shareAshelf}
                onClick={onShareAshelfLinkClick}
              >
                <span className={styles.haveSomethingToContainer}>
                  <span className={styles.aShareSpan}>share</span>
                  <span className={styles.aSpan2}>A</span>
                  <span className={styles.shelfSpan1}>shelf</span>
                </span>
              </a>
            </Link>
            <button className={styles.groupButton} onClick={openMenuDrawer}>
              <div className={styles.groupDiv}>
                <div className={styles.inputChipDiv}>
                  <img
                    className={styles.userImagesUserImages}
                    alt=""
                    src="../user-imagesuser-images.svg"
                  />
                  <div className={styles.labelTextDiv}>Enabled</div>
                </div>
                <img className={styles.menuIcon} alt="" />
                <img className={styles.menuIcon1} alt="" src="../menu.svg" />
              </div>
            </button>
            <h6 className={styles.whatIsShareAshelf}>what is shareAshelf?</h6>
            <Link href="/signup">
              <a className={styles.signUpA} onClick={onSignUpLinkClick}>
                Sign Up:
              </a>
            </Link>
          </div>
          <Link href="/vendor-account">
            <a className={styles.vendorAccount}>Vendor Account</a>
          </Link>
        </header>
      </div>
      {isMenuDrawerOpen && (
        <PortalDrawer
          overlayColor="rgba(113, 113, 113, 0.3)"
          placement="Right"
          onOutsideClick={closeMenuDrawer}
        >
          <MenuDrawer onClose={closeMenuDrawer} />
        </PortalDrawer>
      )}
    </>
  );
};

export default Print;
