import { useState, useCallback } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import MenuDrawer from "../components/menu-drawer";
import PortalDrawer from "../components/portal-drawer";
import styles from "./vendor-payments.module.css";

const VendorPayments = () => {
  const router = useRouter();
  const [isMenuDrawerOpen, setMenuDrawerOpen] = useState(false);

  const onButtonClick = useCallback(() => {
    window.location.href = "/";
  }, []);

  const onShareAshelfLinkClick = useCallback(() => {
    router.push("/");
  }, [router]);

  const openMenuDrawer = useCallback(() => {
    setMenuDrawerOpen(true);
  }, []);

  const closeMenuDrawer = useCallback(() => {
    setMenuDrawerOpen(false);
  }, []);

  const onSignUpLinkClick = useCallback(() => {
    router.push("/signup");
  }, [router]);

  return (
    <>
      <div className={styles.vendorPaymentsDiv}>
        <footer className={styles.footer}>
          <div className={styles.rectangleDiv} />
          <div className={styles.rentmyshelfIncDiv}>
            © 2022 rentmyshelf, Inc.
          </div>
          <div className={styles.supportDiv}>Support</div>
          <div className={styles.contactDiv}>Contact</div>
          <div className={styles.termsOfService}>Terms of Service</div>
          <div className={styles.lineDiv} />
        </footer>
        <div className={styles.lineDiv1} />
        <div className={styles.section2Div}>
          <div className={styles.lineDiv2} />
          <h1 className={styles.earningsH1}>Earnings</h1>
          <h6 className={styles.collectiveTotalsH6}>Collective Totals</h6>
          <h2 className={styles.h2} id="h-week-amount">
            £50
          </h2>
          <p className={styles.thisWeekP}>This week:</p>
          <h2 className={styles.h21} id="h-month-amount">
            £170
          </h2>
          <p className={styles.thisMonthP}>This month:</p>
          <h2 className={styles.h22} id="h-year-amount">
            £2609
          </h2>
          <p className={styles.thisYearP}>This year:</p>
          <h2 className={styles.h23} id="h-total-amount">
            £4380
          </h2>
          <p className={styles.allTimeP}>All time:</p>
          <h6 className={styles.totalsByCollaboration}>
            Totals by Collaboration
          </h6>
          <div className={styles.hColl3Div} id="h-coll3">
            <p className={styles.danFineArt}>Dan Fine Art</p>
            <p className={styles.p} id="pr2">
              £62
            </p>
            <p className={styles.p1}>14/02/2022 - 30/02/2022</p>
          </div>
          <div className={styles.hColl2Div} id="h-coll2">
            <p className={styles.danFineArt}>Ritz Cosmetics</p>
            <p className={styles.p} id="pr3">
              £99
            </p>
            <p className={styles.p1}>21/03/2022 - 23/03/2022</p>
          </div>
          <div className={styles.hColl1Div} id="h-coll1">
            <p className={styles.danFineArt}>Yogel</p>
            <p className={styles.p} id="pr1">
              £59
            </p>
            <p className={styles.p1}>28/04/2022 - 28/05/2022</p>
          </div>
        </div>
        <div className={styles.section1Div}>
          <div className={styles.lineDiv2} />
          <div className={styles.frameDiv}>
            <h2 className={styles.payoutAccountH2}>Payout Account</h2>
            <p className={styles.accountNo3842847389SortCo}>
              <p className={styles.accountNoP}>Account no:</p>
              <p className={styles.accountNoP}>3842847389</p>
              <p className={styles.accountNoP}>Sort Code:</p>
              <p className={styles.p7}>93-95-28</p>
            </p>
            <h2 className={styles.santanderH2}>Santander</h2>
            <button className={styles.button} onClick={onButtonClick}>
              <div className={styles.labelTextDiv}>Edit</div>
            </button>
          </div>
          <div className={styles.frameDiv1}>
            <a className={styles.add} href="/">
              <img className={styles.addIcon} alt="" src="../add1.svg" />
            </a>
          </div>
          <p className={styles.savedBankAccounts}>Saved bank accounts</p>
          <h2 className={styles.earningsH1}>Accounts</h2>
        </div>
        <h1 className={styles.paymentsH1}>Payments</h1>
        <div className={styles.lineDiv4} />
        <div className={styles.headerDiv}>
          <div className={styles.frameDiv2}>
            <Link href="/">
              <a
                className={styles.shareAshelf}
                onClick={onShareAshelfLinkClick}
              >
                <span className={styles.shareAshelfTxtSpan}>
                  <span className={styles.shareSpan}>share</span>
                  <span className={styles.aSpan}>A</span>
                  <span className={styles.shelfSpan}>shelf</span>
                </span>
              </a>
            </Link>
            <button className={styles.groupButton} onClick={openMenuDrawer}>
              <div className={styles.groupDiv}>
                <div className={styles.inputChipDiv}>
                  <img
                    className={styles.userImagesUserImages}
                    alt=""
                    src="../user-imagesuser-images.svg"
                  />
                  <div className={styles.labelTextDiv1}>Enabled</div>
                </div>
                <img className={styles.menuIcon} alt="" />
                <img className={styles.menuIcon1} alt="" src="../menu.svg" />
              </div>
            </button>
            <h6 className={styles.whatIsShareAshelf}>what is shareAshelf?</h6>
            <Link href="/signup">
              <a className={styles.signUpA} onClick={onSignUpLinkClick}>
                Sign Up:
              </a>
            </Link>
          </div>
          <Link href="/vendor-account">
            <a className={styles.vendorAccount}>Vendor Account</a>
          </Link>
        </div>
        <div className={styles.sidebarDiv}>
          <Link href="/vendor-account">
            <a className={styles.profileA}>Profile</a>
          </Link>
          <Link href="/vendor-collaborations">
            <a className={styles.collaborations}>Collaborations</a>
          </Link>
          <Link href="/vendor-account-setting">
            <a className={styles.accountSettings}>Account Settings</a>
          </Link>
          <Link href="/vendor-messages">
            <a className={styles.messages}>Messages</a>
          </Link>
          <a className={styles.payments} href="/">
            Payments
          </a>
        </div>
      </div>
      {isMenuDrawerOpen && (
        <PortalDrawer
          overlayColor="rgba(113, 113, 113, 0.3)"
          placement="Right"
          onOutsideClick={closeMenuDrawer}
        >
          <MenuDrawer onClose={closeMenuDrawer} />
        </PortalDrawer>
      )}
    </>
  );
};

export default VendorPayments;
