import { useState, useCallback } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import MenuDrawer from "../components/menu-drawer";
import PortalDrawer from "../components/portal-drawer";
import styles from "./vendor-messages.module.css";

const VendorMessages = () => {
  const router = useRouter();
  const [isMenuDrawerOpen, setMenuDrawerOpen] = useState(false);

  const onShareAshelfLinkClick = useCallback(() => {
    router.push("/");
  }, [router]);

  const openMenuDrawer = useCallback(() => {
    setMenuDrawerOpen(true);
  }, []);

  const closeMenuDrawer = useCallback(() => {
    setMenuDrawerOpen(false);
  }, []);

  const onSignUpLinkClick = useCallback(() => {
    router.push("/signup");
  }, [router]);

  return (
    <>
      <div className={styles.vendorMessagesDiv}>
        <div className={styles.lineDiv} />
        <div className={styles.lineDiv1} />
        <footer className={styles.groupFooter}>
          <div className={styles.rectangleDiv} />
          <div className={styles.rentmyshelfIncDiv}>
            © 2022 rentmyshelf, Inc.
          </div>
          <div className={styles.supportDiv}>Support</div>
          <div className={styles.contactDiv}>Contact</div>
          <div className={styles.termsOfService}>Terms of Service</div>
          <div className={styles.lineDiv2} />
        </footer>
        <div className={styles.frameDiv}>
          <div className={styles.frameDiv1}>
            <Link href="/">
              <a
                className={styles.shareAshelf}
                onClick={onShareAshelfLinkClick}
              >
                <span className={styles.shareAshelfTxtSpan}>
                  <span className={styles.shareSpan}>share</span>
                  <span className={styles.aSpan}>A</span>
                  <span className={styles.shelfSpan}>shelf</span>
                </span>
              </a>
            </Link>
            <button className={styles.groupButton} onClick={openMenuDrawer}>
              <div className={styles.groupDiv}>
                <div className={styles.inputChipDiv}>
                  <img
                    className={styles.userImagesUserImages}
                    alt=""
                    src="../user-imagesuser-images.svg"
                  />
                  <div className={styles.labelTextDiv}>Enabled</div>
                </div>
                <img className={styles.menuIcon} alt="" />
                <img className={styles.menuIcon1} alt="" src="../menu.svg" />
              </div>
            </button>
            <h6 className={styles.whatIsShareAshelf}>what is shareAshelf?</h6>
            <Link href="/signup">
              <a className={styles.signUpA} onClick={onSignUpLinkClick}>
                Sign Up:
              </a>
            </Link>
          </div>
          <Link href="/vendor-account">
            <a className={styles.vendorAccount}>Vendor Account</a>
          </Link>
        </div>
        <div className={styles.section1Div}>
          <a className={styles.inboxA} href="/">
            Inbox
          </a>
          <a className={styles.sentA} href="/">
            Sent
          </a>
          <Link href="/vendor-inbox">
            <a className={styles.msg3A}>
              <h2 className={styles.shelfInCafe}>Shelf in Cafe</h2>
              <h2 className={styles.theIVYH2}>The IVY</h2>
              <h2 className={styles.h2}>02/04/2022</h2>
            </a>
          </Link>
          <Link href="/vendor-inbox">
            <a className={styles.msg2A}>
              <p className={styles.rESmallShelfInAirbnb}>
                RE: small shelf in airbnb
              </p>
              <p className={styles.jameswilderP}>jameswilder</p>
              <h6 className={styles.h6}>04/04/2022</h6>
            </a>
          </Link>
          <Link href="/vendor-inbox">
            <a className={styles.msg1A}>
              <h2 className={styles.shelfInCafe}>Open Space on Counter</h2>
              <h2 className={styles.theIVYH2}>Alpha Costumes</h2>
              <h2 className={styles.h2}>08/04/2022</h2>
            </a>
          </Link>
         
          <div className={styles.lineDiv00} />
          <div className={styles.lineDiv3} />
          <h1 className={styles.messagesH1}>Messages</h1>
        </div>
        <div className={styles.sidebarDiv}>
          <Link href="/vendor-account">
            <a className={styles.profileA}>Profile</a>
          </Link>
          <Link href="/vendor-collaborations">
            <a className={styles.collaborations}>Collaborations</a>
          </Link>
          <Link href="/vendor-account-setting">
            <a className={styles.accountSettings}>Account Settings</a>
          </Link>
          <a className={styles.messages} href="/">
            Messages
          </a>
          <Link href="/vendor-payments">
            <a className={styles.payments}>Payments</a>
          </Link>
        </div>
      </div>
      {isMenuDrawerOpen && (
        <PortalDrawer
          overlayColor="rgba(113, 113, 113, 0.3)"
          placement="Right"
          onOutsideClick={closeMenuDrawer}
        >
          <MenuDrawer onClose={closeMenuDrawer} />
        </PortalDrawer>
      )}
    </>
  );
};

export default VendorMessages;
