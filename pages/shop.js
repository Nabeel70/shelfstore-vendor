import { useEffect, useState, useCallback } from "react";
import { useRouter } from "next/router";
import Link from "next/link";
import MenuDrawer from "../components/menu-drawer";
import PortalDrawer from "../components/portal-drawer";
import styles from "./shop.module.css";

const Shop = () => {
  const router = useRouter();
  const [isMenuDrawerOpen, setMenuDrawerOpen] = useState(false);

  const onButtonClick = useCallback(() => {
    window.location.href = "/";
  }, []);

  const onButton1Click = useCallback(() => {
    router.push("/vendor-collaborations-edit");
  }, [router]);

  const onButton2Click = useCallback(() => {
    window.location.href = "/";
  }, []);

  const onButton3Click = useCallback(() => {
    router.push("/vendor-collaborations-edit");
  }, [router]);

  const onButton4Click = useCallback(() => {
    router.push("/vendor-collaboration");
  }, [router]);

  const onButton5Click = useCallback(() => {
    router.push("/vendor-collaborations-edit");
  }, [router]);

  const onFrameContainer4Click = useCallback(() => {
    router.push("/vendor-collaborations-edit");
  }, [router]);

  const onButton6Click = useCallback(() => {
    router.push("/print");
  }, [router]);

  const onShareAshelfLinkClick = useCallback(() => {
    router.push("/");
  }, [router]);

  const openMenuDrawer = useCallback(() => {
    setMenuDrawerOpen(true);
  }, []);

  const closeMenuDrawer = useCallback(() => {
    setMenuDrawerOpen(false);
  }, []);

  const onSignUpLinkClick = useCallback(() => {
    router.push("/signup");
  }, [router]);

  return (

    <>
      <div className={styles.shopDiv}>
        <div className={styles.lineDiv} />
        <div className={styles.lineDiv1} />
        <div className={styles.lineDiv2} />
        <div className={styles.lineDiv3} />
        <h1 className={styles.collaborationsH1}>Collaborations</h1>
        <h2 className={styles.shopPageH2}>Shop Page</h2>
        <div className={styles.confirmedCollaborationsDiv}>
          Confirmed Collaborations
        </div>
        <div className={styles.frameDiv}>
          <img
            className={styles.rectangleIcon}
            alt=""
            src="../rectangle-15@2x.png"
          />
          <h6 className={styles.jaimesCafeH6}>Jaime’s Cafe</h6>
          <p className={styles.shelfSpace}>Shelf Space</p>
          <p className={styles.p}>08/04/2022 - 08/05/2022</p>
          <button className={styles.button} onClick={onButtonClick}>
            <div className={styles.labelTextDiv}>Your Shop Page</div>
          </button>
          <button className={styles.button1} onClick={onButton1Click}>
            <div className={styles.labelTextDiv}>Cancel Collaboration</div>
          </button>
        </div>
        <div className={styles.frameDiv1}>
          <img
            className={styles.rectangleIcon}
            alt=""
            src="../rectangle-151@2x.png"
          />
          <h6 className={styles.jaimesCafeH6}>Welcome Bicycles</h6>
          <p className={styles.shelfSpace}>counter space</p>
          <p className={styles.p1}>08/04/2022 - 08/05/2022</p>
          <button className={styles.button} onClick={onButton2Click}>
            <div className={styles.labelTextDiv}>Your Shop Page</div>
          </button>
          <button className={styles.button1} onClick={onButton3Click}>
            <div className={styles.labelTextDiv}>Cancel Collaboration</div>
          </button>
        </div>
        <div className={styles.lineDiv4} />
        <h2 className={styles.sentRequestsH2}>Sent Requests</h2>
        <div className={styles.frameDiv2}>
          <img
            className={styles.rectangleIcon}
            alt=""
            src="../rectangle-152@2x.png"
          />
          <h6 className={styles.jaimesCafeH6}>The Bull</h6>
          <p className={styles.shelfSpace}>wall space left of entrance</p>
          <p className={styles.p1}>21/05/2022 - 23/05/2022</p>
          <button className={styles.button} onClick={onButton4Click}>
            <div className={styles.labelTextDiv}>Cancel Request</div>
          </button>
        </div>
        <div className={styles.lineDiv5} />
        <h2 className={styles.receivedNudgesH2}>Received Nudges</h2>
        <p
          className={styles.makeSureToPrintOffYourIn}
        >{`Make sure to print off your info sign and place it next your products in the space, as this contains information about you and the code needed for shoppers to purchase your items. You can generate this sign at any time by clicking on “Your Products Page” and then clicking on “Generate Info and Payment Printout”. `}</p>
        <p
          className={styles.clickOnYourShopPageToA}
        >{`Click on “Your Shop Page” to add products that will be available to buy, and to access the unique display printout. `}</p>
        <div className={styles.frameDiv3}>
          <img
            className={styles.rectangleIcon3}
            alt=""
            src="../rectangle-153@2x.png"
          />
          <div className={styles.rectangleDiv} />
          <h6 className={styles.woodenPotH6}>Wooden Pot</h6>
          <p className={styles.eachP}>£30 Each</p>
          <button className={styles.button} onClick={onButton5Click}>
            <div className={styles.labelTextDiv}>Edit</div>
          </button>
        </div>
        <div className={styles.frameDiv4} onClick={onFrameContainer4Click}>
          <img className={styles.addIcon} alt="" src="../add.svg" />
        </div>
        <img className={styles.errorIcon} alt="" src="../error.svg" />
        <p className={styles.forAddedRigidityWeSuggest}>
          For added rigidity, we suggest using strong paper or card; either when
          you print out the display sign or as a glued-on backing.
        </p>
        <img className={styles.errorIcon1} alt="" src="../error1.svg" />
        <img className={styles.errorIcon2} alt="" src="../error2.svg" />
        <p className={styles.theseItemsWillShowOnATem}>
          These items will show on a temporary shop page whenever anyone scans
          the QR code on your info sheet, which will allow them to purchase the
          items. There is no limit to the number of items you can add, but
          please only add items which you have put on display.
        </p>
        <footer className={styles.footer}>
          <div className={styles.rentmyshelfIncDiv}>
            © 2022 rentmyshelf, Inc.
          </div>
          <div className={styles.supportDiv}>Support</div>
          <div className={styles.contactDiv}>Contact</div>
          <div className={styles.termsOfService}>Terms of Service</div>
          <div className={styles.lineDiv6} />
        </footer>
        <h6 className={styles.jamiesCafeH6}>Jamie’s Cafe</h6>
        <p className={styles.shelfSpace1}>Shelf Space</p>
        <p className={styles.p3}>08/04/2022 - 08/05/2022</p>
        <button className={styles.button6} onClick={onButton6Click}>
          <b className={styles.labelText}>Display Printout</b>
        </button>
        <h2
          className={styles.aNudgeFromAHostIsASimpl}
        >{`A nudge from a host is a simple way for them to say that they would love to work with you! Feel free to either message them or send over a request for one of their spaces. `}</h2>
        <div className={styles.nudges3Div}>
          <p className={styles.ameerBarbersP}>Ameer Barbers</p>
          <p className={styles.p4}>{`14/02/2022 `}</p>
        </div>
        <div className={styles.nudges2Div}>
          <p className={styles.ameerBarbersP}>Amelie</p>
          <p className={styles.p4}>21/03/2022</p>
        </div>
        <div className={styles.nudges1Div}>
          <p className={styles.ameerBarbersP}>Bob’s Cafe</p>
          <p className={styles.p4}>28/04/2022</p>
        </div>
        <div className={styles.sidebarDiv}>
          <Link href="/vendor-account">
            <a className={styles.profileA}>Profile</a>
          </Link>
          <a className={styles.collaborations} href="/">
            Collaborations
          </a>
          <Link href="/vendor-account-setting">
            <a className={styles.accountSettings}>Account Settings</a>
          </Link>
          <Link href="/vendor-messages">
            <a className={styles.messages}>Messages</a>
          </Link>
          <Link href="/vendor-payments">
            <a className={styles.payments}>Payments</a>
          </Link>
        </div>
        <Link href="/vendor-collaborations">
          <a className={styles.closeA}>
            <img className={styles.closeIcon} alt="" src="../close.svg" />
          </a>
        </Link>
        <header className={styles.header}>
          <div className={styles.frameDiv5}>
            <Link href="/">
              <a
                className={styles.shareAshelf}
                onClick={onShareAshelfLinkClick}
              >
                <span className={styles.shareAshelfTxtSpan}>
                  <span className={styles.shareSpan}>share</span>
                  <span className={styles.aSpan}>A</span>
                  <span className={styles.shelfSpan}>shelf</span>
                </span>
              </a>
            </Link>
            <button className={styles.groupButton} onClick={openMenuDrawer}>
              <div className={styles.groupDiv}>
                <div className={styles.inputChipDiv}>
                  <img
                    className={styles.userImagesUserImages}
                    alt=""
                    src="../user-imagesuser-images.svg"
                  />
                  <div className={styles.labelTextDiv6}>Enabled</div>
                </div>
                <img className={styles.menuIcon} alt="" />
                <img className={styles.menuIcon1} alt="" src="../menu.svg" />
              </div>
            </button>
            <h6 className={styles.whatIsShareAshelf}>what is shareAshelf?</h6>
            <Link href="/signup">
              <a className={styles.signUpA} onClick={onSignUpLinkClick}>
                Sign Up:
              </a>
            </Link>
          </div>
          <Link href="/vendor-account">
            <a className={styles.vendorAccount}>Vendor Account</a>
          </Link>
        </header>
      </div>
      {isMenuDrawerOpen && (
        <PortalDrawer
          overlayColor="rgba(113, 113, 113, 0.3)"
          placement="Right"
          onOutsideClick={closeMenuDrawer}
        >
          <MenuDrawer onClose={closeMenuDrawer} />
        </PortalDrawer>
      )}
    </>
  );
};

export default Shop;
