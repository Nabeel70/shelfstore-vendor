import { useState, useCallback } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import MenuDrawer from "../components/menu-drawer";
import PortalDrawer from "../components/portal-drawer";
import styles from "./vendor-inbox.module.css";

const VendorInbox = () => {
  const router = useRouter();
  const [isMenuDrawerOpen, setMenuDrawerOpen] = useState(false);

  const onShareAshelfLinkClick = useCallback(() => {
    router.push("/");
  }, [router]);

  const openMenuDrawer = useCallback(() => {
    setMenuDrawerOpen(true);
  }, []);

  const closeMenuDrawer = useCallback(() => {
    setMenuDrawerOpen(false);
  }, []);

  const onSignUpLinkClick = useCallback(() => {
    router.push("/signup");
  }, [router]);

  return (
    <>
      <div className={styles.vendorInboxDiv}>
        <div className={styles.lineDiv} />
        <div className={styles.lineDiv1} />
        <footer className={styles.groupFooter}>
          <div className={styles.rectangleDiv} />
          <div className={styles.rentmyshelfIncDiv}>
            © 2022 rentmyshelf, Inc.
          </div>
          <div className={styles.supportDiv}>Support</div>
          <div className={styles.contactDiv}>Contact</div>
          <div className={styles.termsOfService}>Terms of Service</div>
          <div className={styles.lineDiv2} />
        </footer>
        <div className={styles.frameDiv}>
          <div className={styles.frameDiv1}>
            <Link href="/">
              <a
                className={styles.shareAshelf}
                onClick={onShareAshelfLinkClick}
              >
                <span className={styles.shareAshelfTxtSpan}>
                  <span className={styles.shareSpan}>share</span>
                  <span className={styles.aSpan}>A</span>
                  <span className={styles.shelfSpan}>shelf</span>
                </span>
              </a>
            </Link>
            <button className={styles.groupButton} onClick={openMenuDrawer}>
              <div className={styles.groupDiv}>
                <div className={styles.inputChipDiv}>
                  <img
                    className={styles.userImagesUserImages}
                    alt=""
                    src="../user-imagesuser-images.svg"
                  />
                  <div className={styles.labelTextDiv}>Enabled</div>
                </div>
                <img className={styles.menuIcon} alt="" />
                <img className={styles.menuIcon1} alt="" src="../menu.svg" />
              </div>
            </button>
            <h6 className={styles.whatIsShareAshelf}>what is shareAshelf?</h6>
            <Link href="/signup">
              <a className={styles.signUpA} onClick={onSignUpLinkClick}>
                Sign Up:
              </a>
            </Link>
          </div>
          <Link href="/vendor-account">
            <a className={styles.vendorAccount}>Vendor Account</a>
          </Link>
        </div>
        <Link href="/vendor-messages">
          <a className={styles.inboxA}>Inbox</a>
        </Link>
        <a className={styles.sentA} href="/">
          Sent
        </a>

        <div className={styles.lineDiv00} />
        <h1 className={styles.messagesH1}>Messages</h1>
        <div className={styles.sidebarDiv}>
          <div className={styles.profileDiv}>Profile</div>
          <div className={styles.collaborationsDiv}>Collaborations</div>
          <div className={styles.accountSettingsDiv}>Account Settings</div>
          <b className={styles.messagesB}>Messages</b>
          <div className={styles.paymentsDiv}>Payments</div>
        </div>
        <div className={styles.lineDiv3} />
        <div className={styles.inboxDiv}>
          <Link href="/vendor-messages">
            <a className={styles.closeA}>
              <img className={styles.closeIcon} alt="" src="../close.svg" />
            </a>
          </Link>
          <h6 className={styles.subjectREOpenSpaceOnCou}>
            Subject: RE: Open Space on Counter
          </h6>
          <h6 className={styles.sent080420220635}>Sent: 08/04/2022 06:35</h6>
          <p className={styles.helloLookingForwardToHavi}>
            <p className={styles.helloP}>Hello,</p>
            <p className={styles.helloP}>&nbsp;</p>
            <p className={styles.helloP}>
              Looking forward to having you in store! Would you mind coming at
              6:30 when we open up shop?
            </p>
            <p className={styles.helloP}>&nbsp;</p>
            <p className={styles.thanksP}>Thanks!</p>
          </p>
          <p className={styles.subjectOpenSpaceOnCounter}>
            Subject: Open Space on Counter
          </p>
          <p className={styles.fromAlphaCostumes}>From: Alpha Costumes</p>
          <p className={styles.sent070420221235}>Sent: 07/04/2022 12:35</p>
          <p className={styles.whatTimeWouldYouLikeMeTo}>
            <p className={styles.helloP}>
              What time would you like me to come round?
            </p>
            <p className={styles.helloP}>&nbsp;</p>
            <p className={styles.helloP}>Best,</p>
            <p className={styles.thanksP}>Alphacostumes</p>
          </p>
          <h6 className={styles.fromCheckmateH6}>From: Checkmate</h6>
          <div className={styles.lineDiv4} />
          <Link href="/vendor-messages">
            <a className={styles.replyA}>
              <img className={styles.replyIcon} alt="" src="../reply.svg" />
            </a>
          </Link>
        </div>
      </div>
      {isMenuDrawerOpen && (
        <PortalDrawer
          overlayColor="rgba(113, 113, 113, 0.3)"
          placement="Right"
          onOutsideClick={closeMenuDrawer}
        >
          <MenuDrawer onClose={closeMenuDrawer} />
        </PortalDrawer>
      )}
    </>
  );
};

export default VendorInbox;
