import { useState, useCallback } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import MenuDrawer from "../components/menu-drawer";
import PortalDrawer from "../components/portal-drawer";
import styles from "./vendor-profile.module.css";

const VendorProfile = () => {
  const onButtonClick = useCallback(() => {
    window.location.href = "/vendors";
  }, []);

  const router = useRouter();
  const [isMenuDrawerOpen, setMenuDrawerOpen] = useState(false);

  const openMenuDrawer = useCallback(() => {
    setMenuDrawerOpen(true);
  }, []);

  const closeMenuDrawer = useCallback(() => {
    setMenuDrawerOpen(false);
  }, []);

  const onSignUpLinkClick = useCallback(() => {
    router.push("/host-account");
  }, [router]);

  return (
    <>
    <div className={styles.vendorProfileDiv}>
      <img
        className={styles.rectangleIcon}
        alt=""
        src="../rectangle-16@2x.png"
      />
      <div className={styles.alphaCostumesVendor}>
        <b>
          <span>Alpha Costumes </span>
          <span className={styles.span}>{`• `}</span>
        </b>
        <span className={styles.span}>
          <i className={styles.vendorI}>Vendor</i>
        </span>
      </div>
      <h6
        className={styles.alphaCostumesMakesPremiumC}
      >{`Alpha Costumes makes premium cosplay for clients across the world; we’ve been featured in numerous magazines such as Apollo and Sci-Fi. `}</h6>
      <div className={styles.alphacosplaycarrdcoDiv}>alphacosplay.carrd.co</div>
    
      <Link href="/vendors">
      <a className={styles.messageVendor} onClick={onSignUpLinkClick}>
      Message Vendor
      </a>
    </Link>
      <img className={styles.vectorIcon} alt="" src="../vector1.svg" />
      <img className={styles.vectorIcon1} alt="" src="../vector3.svg" />
      <img className={styles.vectorIcon2} alt="" src="../vector.svg" />
      <a className={styles.shareAshelf} href="/start">
        <span className={styles.shareAshelfTxtSpan}>
          <span className={styles.shareSpan}>share</span>
          <span className={styles.aSpan}>A</span>
          <span className={styles.shelfSpan}>shelf</span>
        </span>
      </a>
      <p className={styles.loremIpsumDolorSitAmetCo}>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
        veniam, quis nostrud exercitation ullamco laboris adipiscing elit, sed
        do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
        ad minim veniam, quis nostrud exercitation ullamco laboris.
      </p>
      <img className={styles.ellipseIcon} alt="" src="../ellipse-1@2x.png" />
      <div className={styles.lineDiv} />
      <h6 className={styles.nudgeVendorH6}>Nudge Vendor</h6>
      <p className={styles.letTheVendorKnowThatYoud}>
        Let the vendor know that you’d love to work them! You can send each
        vendor a total of 3 nudges every 6 months.
      </p>
      <div className={styles.clothingCraftsArtist}>
        #Clothing #Crafts #Artist
      </div>

      <p className={styles.whatIsShareAshelf}>what is shareAshelf?</p>
      <Link href="/vendor-account">
      <a className={styles.signUpA} onClick={onSignUpLinkClick}>
      Vendor Account
      </a>
    </Link>
      <button className={styles.button} onClick={onButtonClick}>
        <b className={styles.labelText}>Nudge Vendor</b>
      </button>
      <div className={styles.lineDiv1} />
      <h6 className={styles.upcomingCollaborationsH6}>
        Upcoming Collaborations
      </h6>
      <p className={styles.clickOnTheCollaborationTo}>
        Click on the collaboration to see the map location.
      </p>
      <div className={styles.frameDiv}>
        <img
          className={styles.rectangleIcon1}
          alt=""
          src="../rectangle-15@2x.png"
        />
        <h6 className={styles.checkmateH6}>Checkmate</h6>
        <p className={styles.p}>08/04/2022 - 08/05/2022</p>
      </div>
      <div className={styles.frameDiv1}>
        <img
          className={styles.rectangleIcon1}
          alt=""
          src="../media1@2x.png"
        />
        <h6 className={styles.checkmateH6}>Welcome Bicycles</h6>
        <p className={styles.p1}>08/04/2022 - 08/05/2022</p>
      </div>
      <button className={styles.groupButton} onClick={openMenuDrawer}>
      <div className={styles.groupDiv}>
        <div className={styles.inputChipDiv}>
          <img
            className={styles.userImagesUserImages}
            alt=""
            src="../user-imagesuser-images.svg"
          />
          <div className={styles.labelTextDiv}>Enabled</div>
        </div>
        <img className={styles.menuIcon} alt="" />
        <img className={styles.menuIcon1} alt="" src="../menu.svg" />
      </div>
    </button>

      <div className={styles.footerDiv}>
        <div className={styles.rentmyshelfIncDiv}>© 2022 rentmyshelf, Inc.</div>
        <div className={styles.supportDiv}>Support</div>
        <div className={styles.contactDiv}>Contact</div>
        <div className={styles.termsOfService}>Terms of Service</div>
      </div>
      <div className={styles.lineDiv2} />
    </div>
    {isMenuDrawerOpen && (
      <PortalDrawer
        overlayColor="rgba(113, 113, 113, 0.3)"
        placement="Right"
        onOutsideClick={closeMenuDrawer}
      >
        <MenuDrawer onClose={closeMenuDrawer} />
      </PortalDrawer>
    )}
    </>
  );
};

export default VendorProfile;
