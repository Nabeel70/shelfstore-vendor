import { useEffect, useState, useCallback } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Form } from "react-bootstrap";
import Link from "next/link";
import { useRouter } from "next/router";
import MenuDrawer from "../components/menu-drawer";
import PortalDrawer from "../components/portal-drawer";
import styles from "./vendor-account.module.css";

const VendorAccount = () => {
  const [val,setVal]=useState([]);
  const handleAdd=()=>{
      const abc=[...val,[]]
      setVal(abc)
  }
  const handleChange=(onChangeValue,j)=>{
   const inputdata=[...val]
   inputdata[j]=onChangeValue.target.value;
   setVal(inputdata)
  }
  const handleDelete=(j)=>{
      const deletVal=[...val]
      deletVal.splice(j,1)
      setVal(deletVal)  
  }
  console.log(val,"data-")

  const router = useRouter();
  const [isMenuDrawerOpen, setMenuDrawerOpen] = useState(false);

  const onShareAshelfLinkClick = useCallback(() => {
    router.push("/");
  }, [router]);

  const openMenuDrawer = useCallback(() => {
    setMenuDrawerOpen(true);
  }, []);

  const closeMenuDrawer = useCallback(() => {
    setMenuDrawerOpen(false);
  }, []);

  const onSignUpLinkClick = useCallback(() => {
    router.push("/signup");
  }, [router]);

  return (

    useEffect(() => {

      const image_inp = document.querySelector('#image_input');
var uploaded_image = "";
image_inp.addEventListener("change", function(){
    const reader = new FileReader();
    reader.addEventListener("load", () => {
      uploaded_image = reader.result;
      document.querySelector("#display_image").style.backgroundImage = `url(${uploaded_image})`
    });
    reader.readAsDataURL(this.files[0]);
});

const cover_inp = document.querySelector('#cover_input');
var uploaded_image = "";
cover_inp.addEventListener("change", function(){
    const reader = new FileReader();
    reader.addEventListener("load", () => {
      uploaded_image = reader.result;
      document.querySelector("#cover-photo").style.backgroundImage = `url(${uploaded_image})`
    });
    reader.readAsDataURL(this.files[0]);
});

const upl = document.querySelector('#upload');
var uploaded_image = "";
upl.addEventListener("change", function(){
    const reader = new FileReader();
    reader.addEventListener("load", () => {
      uploaded_image = reader.result;
      document.querySelector("#upload-photo").style.backgroundImage = `url(${uploaded_image})`
    });
    reader.readAsDataURL(this.files[0]);
});

const upl1 = document.querySelector('#upload2');
var uploaded_image = "";
upl1.addEventListener("change", function(){
    const reader = new FileReader();
    reader.addEventListener("load", () => {
      uploaded_image = reader.result;
      document.querySelector("#upload-photo1").style.backgroundImage = `url(${uploaded_image})`
    });
    reader.readAsDataURL(this.files[0]);
});

const upl2 = document.querySelector('#upload1');
var uploaded_image = "";
upl2.addEventListener("change", function(){
    const reader = new FileReader();
    reader.addEventListener("load", () => {
      uploaded_image = reader.result;
      document.querySelector("#upload-photo2").style.backgroundImage = `url(${uploaded_image})`
    });
    reader.readAsDataURL(this.files[0]);
});

}),

    <>
      <div className={styles.vendorAccountDiv}>
        <div className={styles.sidebarDiv}>
          <a className={styles.profileA} href="/">
            Profile
          </a>
          <Link href="/vendor-collaborations">
            <a className={styles.collaborations}>Collaborations</a>
          </Link>
          <Link href="/vendor-account-setting">
            <a className={styles.accountSettings}>Account Settings</a>
          </Link>
          <Link href="/vendor-messages">
            <a className={styles.messages}>Messages</a>
          </Link>
          <Link href="/vendor-payments">
            <a className={styles.payments}>Payments</a>
          </Link>
        </div>
        <div className={styles.lineDiv} />
        <div className={styles.lineDiv1} />
        <h1 className={styles.profileH1}>Profile</h1>
        <p
          className={styles.theDistrictCodeOfYourPost}
        >{`The district code of your postcode (the first set of letters followed by the first set of numbers) i.e. SE11 or IV30. This is to help people find you on a map, and get a general sense of your location without compromising your privacy. `}</p>
        <div className={styles.lineDiv2} />
        <h2 className={styles.localAreaH2}>Local Area</h2>
        <h2 className={styles.vendorTypeH2}>Vendor Type</h2>
        <p className={styles.selectUpToThreeCategories}>
          Select Up to Three Categories that Best Represent You:
        </p>
        <div className={styles.lineDiv3} />
        <p className={styles.trailingDataP}>Food</p>
        <p className={styles.trailingDataP1}>Drinks</p>
        <Form.Check className={styles.trailingIconFormCheck} isInvalid />
        <Form.Check className={styles.trailingIconFormCheck1} isInvalid />
        <p className={styles.trailingDataP2}>Clothing</p>
        <Form.Check className={styles.trailingIconFormCheck2} isInvalid />
        <Form.Check className={styles.trailingIconFormCheck3} isInvalid />
        <Form.Check className={styles.trailingIconFormCheck4} isInvalid />
        <div className={styles.trailingDataDiv}>Cosmetics</div>
        <div className={styles.trailingDataDiv1}>Art and Crafts</div>
        <Form.Check className={styles.trailingIconFormCheck5} isInvalid />
        <div className={styles.trailingDataDiv2}>Souvenirs</div>
        <p className={styles.shortBioThisIsDisplayed}>
          Short Bio - this is displayed on the map pop-ups and at the top of
          your profile.
        </p>
        <div className={styles.lineDiv4} />
        <div className={styles.lineDiv5} />
        <h2 className={styles.imagesH2}>Images</h2>
        <input
          className={styles.rectangleInput}
          type="text"
          placeholder="0/30"
          maxLength={30}
          minLength={0}
        />
        <p className={styles.mainImageThisIsTheMain}>
          Main image - This is the main image at the top of your profile.
        </p>
        <p className={styles.profilePictureThisIsDisp}>
          Profile Picture - This is displayed on your profile beside your name,
          as well as on the pop-ups found on the map.
        </p>
        <div className={styles.lineDiv6} />
        <h2 className={styles.socialMediaH2}>Social Media</h2>
        <p className={styles.linkYourSocialMediaChannel}>
          Link your social media channels here to help people find you.
        </p>
        <p className={styles.taglineDescribeWhoYouAre}>
          Tagline - describe who you are in 10 words or less; this is displayed
          alongside your name on map pop-ups.
        </p>
        <input
          className={styles.rectangleInput1}
          type="text"
          placeholder="0/10"
          maxLength={10}
          minLength={0}
        />
        <div className={styles.lineDiv7} />
        <h2 className={styles.websiteH2}>Website</h2>
        <h2 className={styles.bioH2}>Bio</h2>
        <p className={styles.ifYouHaveAWebsiteFeelFr}>
          If you have a website, feel free to link to it here.
        </p>
        <input
          className={styles.rectangleInput2}
          type="text"
          placeholder="alphacosplay.carrd.co"
        />
        <input
          className={styles.rectangleInput3}
          type="text"
          placeholder="NW1"
        />
        <input
          className={styles.rectangleInput4}
          type="text"
          placeholder="instagram.com/checkmatecafe"
        />
        <p className={styles.longBioThisIsDisplayedO}>
          Long Bio - this is displayed on your profile as extra information to
          coomplement the short bio.
        </p>
        <input
          className={styles.rectangleInput5}
          type="text"
          placeholder="0/300"
          maxLength={300}
          minLength={0}
        />
        <div
          className={styles.rectangleIcon}
          alt=""
          id="cover-photo"
          
        />
        
        <input className={styles.buttonInput} type="file" id="cover_input"  hidden/>
        <label className={styles.buttonInput} for="cover_input">Edit</label>

        <p className={styles.extraImagesAddUpToThree}>
          Extra Images - add up to three extra images; these are displayed as
          part of the carousel at the top of your profile.
        </p>
        <div id="upload-photo" className={styles.rectangleDiv} />
        <div id="upload-photo1" className={styles.rectangleDiv1} />
        <div id="upload-photo2" className={styles.rectangleDiv2} />
        <div id="display_image" className={styles.frameDiv}>
          <button className={styles.button}>
      
        <input className={styles.labelTextDiv} accept="image/png, image/jpg, image/svg" id="image_input" type="file" hidden/>
        <label className={styles.labelTextDiv} for="image_input">Edit</label>
           
          </button>
        </div>
        <input
        className={styles.rectangleInput4}
        type="text"
        placeholder="instagram.com/checkmatecafe"
      />

      
        <button onClick={()=>handleAdd()}
        className={styles.button1}
        id="add-link" >
          <div onClick={()=>handleAdd()} className={styles.addAnotherDiv}>Add Another</div>
          <div className={styles.rectangleDiv3} />
        </button>
        <img className={styles.vectorIcon} alt="" src="../vector1.svg" />
        <input
          className={styles.rectangleInput6}
          type="text"
          placeholder="tiktok.com/checkmatecafe"
        />
        <input
          className={styles.rectangleInput7}
          type="text"
          placeholder="https://www.facebook.com/checkmatecafe"
        />

        <input className={styles.addInput} type="file" id="upload" hidden/>
        <label className={styles.addInput} for="upload">+</label>
        
        <input className={styles.add2Input} type="file" id="upload1" hidden/>
        <label className={styles.add2Input} for="upload1">+</label>

        <input className={styles.addInput1} type="file" id="upload2" hidden />
        <label className={styles.addInput1} for="upload2">+</label>

        <img className={styles.vectorIcon1} alt="" src="../vector2.svg" />
        <img className={styles.vectorIcon2} alt="" src="../vector3.svg" />
        <footer className={styles.footer}>
          <div className={styles.rentmyshelfIncDiv}>
            © 2022 rentmyshelf, Inc.
          </div>
          <div className={styles.supportDiv}>Support</div>
          <div className={styles.contactDiv}>Contact</div>
          <div className={styles.termsOfService}>Terms of Service</div>
          <div className={styles.lineDiv8} />
        </footer>
        <header className={styles.header}>
          <div className={styles.frameDiv1}>
            <Link href="/">
              <a
                className={styles.shareAshelf}
                onClick={onShareAshelfLinkClick}
              >
                <span className={styles.shareAshelfTxtSpan}>
                  <span className={styles.shareSpan}>share</span>
                  <span className={styles.aSpan}>A</span>
                  <span className={styles.shelfSpan}>shelf</span>
                </span>
              </a>
            </Link>
            <button className={styles.groupButton} onClick={openMenuDrawer}>
              <div className={styles.groupDiv}>
                <div className={styles.inputChipDiv}>
                  <img
                    className={styles.userImagesUserImages}
                    alt=""
                    src="../user-imagesuser-images.svg"
                  />
                  <div className={styles.labelTextDiv1}>Enabled</div>
                </div>
                <img className={styles.menuIcon} alt="" />
                <img className={styles.menuIcon1} alt="" src="../menu.svg" />
              </div>
            </button>
            <h6 className={styles.whatIsShareAshelf}>what is shareAshelf?</h6>
            <Link href="/signup">
              <a className={styles.signUpA} onClick={onSignUpLinkClick}>
                Sign Up:
              </a>
            </Link>
          </div>
          <a className={styles.vendorAccount} href="/">
            Vendor Account
          </a>
        </header>
      </div>
      {isMenuDrawerOpen && (
        <PortalDrawer
          overlayColor="rgba(113, 113, 113, 0.3)"
          placement="Right"
          onOutsideClick={closeMenuDrawer}
        >
          <MenuDrawer onClose={closeMenuDrawer} />
        </PortalDrawer>
      )}
    </>
  );
};

export default VendorAccount;
