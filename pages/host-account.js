import {useEffect, Fragment, useState, useCallback } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Form } from "react-bootstrap";
import Link from "next/link";
import { useRouter } from "next/router";
import MenuDrawer from "../components/menu-drawer";
import PortalDrawer from "../components/portal-drawer";
import styles from "./host-account.module.css";
// import "./images";

const HostAccount = () => {
  const [val,setVal]=useState([]);
  const handleAdd=()=>{
      const abc=[...val,[]]
      setVal(abc)
  }
  const handleChange=(onChangeValue,j)=>{
   const inputdata=[...val]
   inputdata[j]=onChangeValue.target.value;
   setVal(inputdata)
  }
  const handleDelete=(j)=>{
      const deletVal=[...val]
      deletVal.splice(j,1)
      setVal(deletVal)  
  }
  console.log(val,"data-")

  const router = useRouter();
  const [isMenuDrawerOpen, setMenuDrawerOpen] = useState(false);

  const onMySpacesForClick = useCallback(() => {
    router.push("/my-spaces");
  }, [router]);

  const onButtonClick = useCallback(() => {
    router.push("/edit-my-spaces");
  }, [router]);

  const onAddButtonClick = useCallback(() => {
    router.push("/edit-my-spaces");
  }, [router]);

  const onRectangleButtonClick = useCallback(() => {
    window.location.href = "/";
  }, []);

  const onShareAshelfLinkClick = useCallback(() => {
    router.push("/");
  }, [router]);

  const openMenuDrawer = useCallback(() => {
    setMenuDrawerOpen(true);
  }, []);

  const closeMenuDrawer = useCallback(() => {
    setMenuDrawerOpen(false);
  }, []);

  const onSignUpLinkClick = useCallback(() => {
    router.push("/signup");
  }, [router]);
  

  return (
    useEffect(() => {

      const background= document.querySelector('#last_input');
var done_image = "";
background.addEventListener("change", function(){
    const reader = new FileReader();
    reader.addEventListener("load", () => {
      uploaded_image = reader.result;
      document.querySelector("#cover-last").style.backgroundImage = `url(${done_image})`
    });
    reader.readAsDataURL(this.files[0]);
});

      const image_input = document.querySelector('#image_input');
var uploaded_image = "";
image_input.addEventListener("change", function(){
    const reader = new FileReader();
    reader.addEventListener("load", () => {
      uploaded_image = reader.result;
      document.querySelector("#display_image").style.backgroundImage = `url(${uploaded_image})`
    });
    reader.readAsDataURL(this.files[0]);
});

const cover_input = document.querySelector('#cover_input');
var uploaded_image = "";
cover_input.addEventListener("change", function(){
    const reader = new FileReader();
    reader.addEventListener("load", () => {
      uploaded_image = reader.result;
      document.querySelector("#cover-photo").style.backgroundImage = `url(${uploaded_image})`
    });
    reader.readAsDataURL(this.files[0]);
});

const cover_input1 = document.querySelector('#cover_input1');
var uploaded_image = "";
cover_input1.addEventListener("change", function(){
    const reader = new FileReader();
    reader.addEventListener("load", () => {
      uploaded_image = reader.result;
      document.querySelector("#cover-photo1").style.backgroundImage = `url(${uploaded_image})`
    });
    reader.readAsDataURL(this.files[0]);
});

const cover_input2 = document.querySelector('#cover_input2');
var uploaded_image = "";
cover_input2.addEventListener("change", function(){
    const reader = new FileReader();
    reader.addEventListener("load", () => {
      uploaded_image = reader.result;
      document.querySelector("#cover-photo2").style.backgroundImage = `url(${uploaded_image})`
    });
    reader.readAsDataURL(this.files[0]);
});


}),
    <>
      <div className={styles.hostAccountDiv}>
        <div className={styles.sidebarDiv}>
          <a className={styles.profileA} href="/">
            Profile
          </a>
          <Link href="/host-collaborations">
            <a className={styles.collaborations}>Collaborations</a>
          </Link>
          <Link href="/my-spaces">
            <a className={styles.mySpacesForRent} onClick={onMySpacesForClick}>
              My Spaces for Rent
            </a>
          </Link>
          <Link href="/host-account-setting">
            <a className={styles.accountSettings}>Account Settings</a>
          </Link>
          <Link href="/host-messages">
            <a className={styles.messages}>Messages</a>
          </Link>
          <Link href="/host-payments">
            <a className={styles.payments}>Payments</a>
          </Link>
        </div>
        <div className={styles.lineDiv} />
        <h1 className={styles.profileH1}>Profile</h1>
        <div className={styles.section3Div}>
          <div className={styles.lineDiv1} />
          <h1 className={styles.websiteH1}>Website</h1>
          <p className={styles.ifYouHaveAWebsiteFeelFr}>
            If you have a website, feel free to link to it here.
          </p>
          <input
            className={styles.rectangleInput}
            type="text"
            placeholder="checkmateedinburgh.com"
          />
          <div className={styles.checkmateedinburghcomDiv}>
            checkmateedinburgh.com
          </div>
        </div>
        <div className={styles.section4Div}>
          <p className={styles.shortBioThisIsDisplayed}>
            Short Bio - this is displayed on the map pop-ups and at the top of
            your profile.
          </p>
          <div className={styles.lineDiv1} />
          <h1 className={styles.websiteH1}>Bio</h1>
          <input
            className={styles.rectangleInput1}
            type="text"
            placeholder="0/30"
            maxLength={30}
            minLength={0}
          />
          <p className={styles.ifYouHaveAWebsiteFeelFr}>
            Tagline - describe who you are in 10 words or less; this is
            displayed alongside your name on map pop-ups.
          </p>
          <input
            className={styles.rectangleInput}
            type="text"
            placeholder="0/10"
            maxLength={10}
            minLength={0}
          />
          <p className={styles.longBioThisIsDisplayedO}>
            Long Bio - this is displayed on your profile as extra information to
            coomplement the short bio.
          </p>
          <input
            className={styles.rectangleInput3}
            type="text"
            placeholder="0/300"
            maxLength={300}
            minLength={0}
          />
        </div>
        <div className={styles.section2Div}>
          <div className={styles.lineDiv1} />
          <h1 className={styles.hostTypeH1}>Host Type</h1>
          <p className={styles.trailingDataP}>Cafe</p>
          <Form.Check className={styles.trailingIconFormCheck} isInvalid />
          <p className={styles.trailingDataP1}>Restaurant</p>
          <Form.Check className={styles.trailingIconFormCheck1} isInvalid />
          <p className={styles.trailingDataP2}>Venue</p>
          <Form.Check className={styles.trailingIconFormCheck2} isInvalid />
          <p className={styles.trailingDataP3}>Community</p>
          <p className={styles.trailingDataP4}>Studios</p>
          <Form.Check className={styles.trailingIconFormCheck3} isInvalid />
          <Form.Check className={styles.trailingIconFormCheck4} isInvalid />
          <Form.Check className={styles.trailingIconFormCheck5} isInvalid />
          <p className={styles.trailingDataP5}>Co-Work</p>
          <p className={styles.selectUpToThreeCategories}>
            Select Up to Three Categories that Best Represent You:
          </p>
        </div>
        <div className={styles.lineDiv4} />
        <div className={styles.section1Div}>
          <p className={styles.addressesOfYourSpaces}>
            Address(es) of your space(s):
          </p>
          <div className={styles.lineDiv1} />
          <h2 className={styles.locationsH2}>Location(s)</h2>
          <div className={styles.frameDiv}>
            <h3 className={styles.burnbankBurnsideRoadKingsto}>
              <p className={styles.burnbankP}>Burnbank</p>
              <p className={styles.burnbankP}>Burnside Road</p>
              <p className={styles.burnbankP}>Kingston</p>
              <p className={styles.iV327NYP}>IV32 7NY</p>
            </h3>
            <h2 className={styles.burnsideRoadH2}>Burnside Road</h2>
            <button className={styles.button} onClick={onButtonClick}>
              <div className={styles.labelTextDiv}>Edit</div>
            </button>
          </div>
          <div className={styles.frameDiv1}>
            <button className={styles.addButton} onClick={onAddButtonClick}>
              <img className={styles.addIcon} alt="" src="../add.svg" />
            </button>
          </div>
        </div>
        <div className={styles.section6Div}>
          <div className={styles.lineDiv1} />
          <h1 className={styles.locationsH2}>Images</h1>
          <p className={styles.mainImageThisIsTheMain}>
            Main image - This is the main image at the top of your profile.
          </p>
          <p className={styles.profilePictureThisIsDisp}>
            Profile Picture - This is displayed on your profile beside your
            name, as well as on the pop-ups found on the map.
          </p>
         
          <div id="cover-photo1" className={styles.rectangleDiv} />
          <input id="cover_input1" accept="image/png, image/jpg, image/svg" className={styles.addInput} type="file" />
          
          <div id="cover-photo2" className={styles.rectangleDiv1} />
          <input id="cover_input2" accept="image/png, image/jpg, image/svg" className={styles.addInput1} type="file" />

          <div id="cover_last" className={styles.rectangleDiv2} />
          <input id="last_input" accept="image/png, image/jpg, image/svg" className={styles.addInput2} type="file" />

      
      
          <div className={styles.rectangleIcon}
          id="cover-photo">
          </div>
          <input id="cover_input" className={styles.buttonInput} type="file" />
          <p className={styles.extraImagesAddUpToThree}>
          Extra Images - add up to three extra images; these are displayed as
          part of the carousel at the top of your profile.
        </p>
          
          <div id="display_image" className={styles.frameDiv2}>
            <input className={styles.buttonInput1} accept="image/png, image/jpg, image/svg" id="image_input" type="file" autoFocus />
          </div>
        </div>

   
    
      
        <footer className={styles.footer}>
          <div className={styles.rentmyshelfIncDiv}>
            © 2022 rentmyshelf, Inc.
          </div>
          <div className={styles.supportDiv}>Support</div>
          <div className={styles.contactDiv}>Contact</div>
          <div className={styles.termsOfService}>Terms of Service</div>
          <div className={styles.lineDiv7} />
        </footer>
        <div className={styles.section5Div}>
          <div className={styles.lineDiv1} />
          <h1 className={styles.locationsH2}>Social Media</h1>
          <p className={styles.profilePictureThisIsDisp}>
            Link your social media channels here to help people find you.
          </p>
          

        {val.map((data,j)=>{
            return(
               <div>
                    <input className={styles.rectangleInput00} value={data} onChange={e=>handleChange(e,j)}
            type="text"
            placeholder="instagram.com/checkmatecafe" /><br/>
            <button className={styles.rectangleInput01} onClick={()=>handleDelete(j)}>x</button>
               </div>
            )
        })}

          <img className={styles.vectorIcon} alt="" src="../vector.svg" />
          <input
            className={styles.rectangleInput4}
            type="text"
            placeholder="instagram.com/checkmatecafe"
          />
          <div onClick={()=>handleAdd()} className={styles.addAnotherDiv}>Add Another</div>
          <button onClick={()=>handleAdd()}
            className={styles.rectangleButton}
            id="add-link"
          />
        </div>
        <div className={styles.frameDiv3}>
          <Link href="/">
            <a className={styles.shareAshelf} onClick={onShareAshelfLinkClick}>
              <span className={styles.shareAshelfTxtSpan}>
                <span className={styles.shareSpan}>share</span>
                <span className={styles.aSpan}>A</span>
                <span className={styles.shelfSpan}>shelf</span>
              </span>
            </a>
          </Link>
          <button className={styles.groupButton} onClick={openMenuDrawer}>
            <div className={styles.groupDiv}>
              <div className={styles.inputChipDiv}>
                <img
                  className={styles.userImagesUserImages}
                  alt=""
                  src="../user-imagesuser-images.svg"
                />
                <div className={styles.labelTextDiv1}>Enabled</div>
              </div>
              <img className={styles.menuIcon} alt="" />
              <img className={styles.menuIcon1} alt="" src="../menu.svg" />
            </div>
          </button>
          <h6 className={styles.whatIsShareAshelf}>what is shareAshelf?</h6>
          <Link href="/signup">
            <a className={styles.signUpA} onClick={onSignUpLinkClick}>
              Sign Up:
            </a>
          </Link>
        </div>
        <a className={styles.hostAccount} href="/">
          Host Account
        </a>
      </div>
      {isMenuDrawerOpen && (
        <PortalDrawer
          overlayColor="rgba(113, 113, 113, 0.3)"
          placement="Right"
          onOutsideClick={closeMenuDrawer}
        >
          <MenuDrawer onClose={closeMenuDrawer} />
        </PortalDrawer>
      )}
    </>
  );
};

export default HostAccount;
