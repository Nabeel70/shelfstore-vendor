import { useState, useCallback } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import MenuDrawer from "../components/menu-drawer";
import PortalDrawer from "../components/portal-drawer";
import styles from "./vendor-collaboration.module.css";

const VendorCollaboration = () => {
  const router = useRouter();
  const [isMenuDrawerOpen, setMenuDrawerOpen] = useState(false);

  const onButtonClick = useCallback(() => {
    router.push("/vendor-collaborations");
  }, [router]);

  const onButton1Click = useCallback(() => {
    router.push("/shop");
  }, [router]);

  const onButton2Click = useCallback(() => {
    router.push("/vendor-collaborations-edit");
  }, [router]);

  const onButton3Click = useCallback(() => {
    router.push("/shop");
  }, [router]);

  const onButton4Click = useCallback(() => {
    router.push("/vendor-collaborations-edit");
  }, [router]);

  const onShareAshelfLinkClick = useCallback(() => {
    router.push("/");
  }, [router]);

  const openMenuDrawer = useCallback(() => {
    setMenuDrawerOpen(true);
  }, []);

  const closeMenuDrawer = useCallback(() => {
    setMenuDrawerOpen(false);
  }, []);

  const onSignUpLinkClick = useCallback(() => {
    router.push("/signup");
  }, [router]);

  return (
    <>
      <div className={styles.vendorCollaborationDiv}>
        <div className={styles.footerDiv}>
          <div className={styles.rectangleDiv} />
          <div className={styles.rentmyshelfIncDiv}>
            © 2022 rentmyshelf, Inc.
          </div>
          <div className={styles.supportDiv}>Support</div>
          <div className={styles.contactDiv}>Contact</div>
          <div className={styles.termsOfService}>Terms of Service</div>
          <div className={styles.lineDiv} />
        </div>
        <div className={styles.sidebarDiv}>
          <Link href="/vendor-account">
            <a className={styles.profileA}>Profile</a>
          </Link>
          <a className={styles.collaborations} href="/">
            Collaborations
          </a>
          <Link href="/vendor-account-setting">
            <a className={styles.accountSettings}>Account Settings</a>
          </Link>
          <Link href="/vendor-messages">
            <a className={styles.messages}>Messages</a>
          </Link>
          <Link href="/vendor-payments">
            <a className={styles.payments}>Payments</a>
          </Link>
        </div>
        <div className={styles.lineDiv1} />
        <div className={styles.section3Div}>
          <h2 className={styles.receivedNudgesH2}>Received Nudges</h2>
          <p
            className={styles.aNudgeFromAHostIsASimpl}
          >{`A nudge from a host is a simple way for them to say that they would love to work with you! Feel free to either message them or send over a request for one of their spaces. `}</p>
          <p className={styles.bobsCafeP}>Bob’s Cafe</p>
          <p className={styles.p}>28/04/2022</p>
          <p className={styles.amelieP}>Amelie</p>
          <p className={styles.p1}>21/03/2022</p>
          <p className={styles.ameerBarbersP}>Ameer Barbers</p>
          <p className={styles.p2}>{`14/02/2022 `}</p>
        </div>
        <div className={styles.lineDiv2} />
        <div className={styles.section2Div}>
          <h2 className={styles.sentRequestsH2}>Sent Requests</h2>
          <div className={styles.frameDiv}>
            <button className={styles.button} onClick={onButtonClick}>
              <div className={styles.labelTextDiv}>Cancel Request</div>
            </button>
            <h6 className={styles.confirmCancellationH6}>
              Confirm Cancellation?
            </h6>
            <h6 className={styles.theBullH6}>The Bull</h6>
            <p className={styles.wallSpaceLeftOfEntrance}>
              wall space left of entrance
            </p>
            <p className={styles.p3}>21/05/2022 - 23/05/2022</p>
            <Link href="/vendor-collaborations">
              <a className={styles.closeA}>
                <img className={styles.closeIcon} alt="" src="../close2.svg" />
              </a>
            </Link>
          </div>
        </div>
        <div className={styles.lineDiv3} />
        <div className={styles.section1Div}>
          <p
            className={styles.makeSureToPrintOffYourIn}
          >{`Make sure to print off your info sign and place it next your products in the space, as this contains information about you and the code needed for shoppers to purchase your items. You can generate this sign at any time by clicking on “Your Products Page” and then clicking on “Generate Info and Payment Printout”. `}</p>
          <p
            className={styles.clickOnYourProductsPage}
          >{`Click on “Your Products Page” to add products that will be on display. `}</p>
          <img className={styles.errorIcon} alt="" src="../error5.svg" />
          <img className={styles.errorIcon1} alt="" src="../error6.svg" />
          <div className={styles.frameDiv1}>
            <img
              className={styles.rectangleIcon}
              alt=""
              src="../rectangle-157@2x.png"
            />
            <h6 className={styles.jaimesCafeH6}>Jaime’s Cafe</h6>
            <p className={styles.shelfSpace}>Shelf Space</p>
            <p className={styles.p4}>08/04/2022 - 08/05/2022</p>
            <button className={styles.button1} onClick={onButton1Click}>
              <div className={styles.labelTextDiv}>Your Shop Page</div>
            </button>
            <button className={styles.button2} onClick={onButton2Click}>
              <div className={styles.labelTextDiv}>Cancel Collaboration</div>
            </button>
          </div>
          <div className={styles.frameDiv2}>
            <img
              className={styles.rectangleIcon}
              alt=""
              src="../rectangle-151@2x.png"
            />
            <h2 className={styles.jaimesCafeH6}>Welcome Bicycles</h2>
            <p className={styles.shelfSpace}>counter space</p>
            <p className={styles.p5}>08/04/2022 - 08/05/2022</p>
            <button className={styles.button1} onClick={onButton3Click}>
              <div className={styles.labelTextDiv}>Your Shop Page</div>
            </button>
            <button className={styles.button2} onClick={onButton4Click}>
              <div className={styles.labelTextDiv}>Cancel Collaboration</div>
            </button>
          </div>
        </div>
        <div className={styles.confirmedCollaborationsDiv}>
          Confirmed Collaborations
        </div>
        <div className={styles.lineDiv4} />
        <h1 className={styles.collaborationsH1}>Collaborations</h1>
        <div className={styles.lineDiv5} />
        <header className={styles.header}>
          <div className={styles.frameDiv3}>
            <Link href="/">
              <a
                className={styles.shareAshelf}
                onClick={onShareAshelfLinkClick}
              >
                <span className={styles.shareAshelfTxtSpan}>
                  <span className={styles.shareSpan}>share</span>
                  <span className={styles.aSpan}>A</span>
                  <span className={styles.shelfSpan}>shelf</span>
                </span>
              </a>
            </Link>
            <button className={styles.groupButton} onClick={openMenuDrawer}>
              <div className={styles.groupDiv}>
                <div className={styles.inputChipDiv}>
                  <img
                    className={styles.userImagesUserImages}
                    alt=""
                    src="../user-imagesuser-images.svg"
                  />
                  <div className={styles.labelTextDiv5}>Enabled</div>
                </div>
                <img className={styles.menuIcon} alt="" />
                <img className={styles.menuIcon1} alt="" src="../menu.svg" />
              </div>
            </button>
            <h6 className={styles.whatIsShareAshelf}>what is shareAshelf?</h6>
            <Link href="/signup">
              <a className={styles.signUpA} onClick={onSignUpLinkClick}>
                Sign Up:
              </a>
            </Link>
          </div>
          <Link href="/vendor-account">
            <a className={styles.vendorAccount}>Vendor Account</a>
          </Link>
        </header>
      </div>
      {isMenuDrawerOpen && (
        <PortalDrawer
          overlayColor="rgba(113, 113, 113, 0.3)"
          placement="Right"
          onOutsideClick={closeMenuDrawer}
        >
          <MenuDrawer onClose={closeMenuDrawer} />
        </PortalDrawer>
      )}
    </>
  );
};

export default VendorCollaboration;
