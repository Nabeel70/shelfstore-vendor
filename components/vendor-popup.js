import { useCallback } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import styles from "./vendor-popup.module.css";

const VendorPopup = ({ onClose }) => {
  const router = useRouter();

  const onFrameLinkClick = useCallback(() => {
    router.push("/vendor-profile");
  }, [router]);

  return (
    <div className={styles.vendorPopupDiv}>
      <h6 className={styles.vendorsInSW1}>Vendors in SW1</h6>
      <Link href="/vendor-profile">
        <a className={styles.frameA} onClick={onFrameLinkClick}>
          <div className={styles.frameDiv}>
            <img
              className={styles.rectangleIcon}
              alt=""
              src="../rectangle-153@2x.png"
            />
            <h2 className={styles.ameliaFineArt}>Amelia Fine Art</h2>
          </div>
          <div className={styles.frameDiv1}>
            <img
              className={styles.rectangleIcon}
              alt=""
              src="../rectangle-154@2x.png"
            />
            <h2 className={styles.ameliaFineArt}>lucky ceramics</h2>
          </div>
          <div className={styles.frameDiv2}>
            <img
              className={styles.rectangleIcon}
              alt=""
              src="../rectangle-153@2x.png"
            />
            <h2 className={styles.ameliaFineArt}>Amelia Fine Art</h2>
          </div>
          <div className={styles.frameDiv3}>
            <img
              className={styles.rectangleIcon}
              alt=""
              src="../rectangle-154@2x.png"
            />
            <h2 className={styles.ameliaFineArt}>lucky ceramics</h2>
          </div>
          <div className={styles.frameDiv4}>
            <img
              className={styles.rectangleIcon4}
              alt=""
              src="../rectangle-157@2x.png"
            />
            <b className={styles.luckyCeramicsB}>Amelia Fine Art</b>
          </div>
          <div className={styles.frameDiv5}>
            <img
              className={styles.rectangleIcon}
              alt=""
              src="../rectangle-158@2x.png"
            />
            <b className={styles.luckyCeramicsB}>lucky ceramics</b>
          </div>
        </a>
      </Link>
      <p className={styles.vendorsMatchTheFilters}>
        13 Vendors Match the Filters
      </p>
      <a className={styles.closeA} onClick={onClose}>
        <img className={styles.closeIcon} alt="" src="../close.svg" />
      </a>
    </div>
  );
};

export default VendorPopup;
