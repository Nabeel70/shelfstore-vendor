"use strict";
/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(() => {
var exports = {};
exports.id = "pages/test2";
exports.ids = ["pages/test2"];
exports.modules = {

/***/ "./pages/test.js":
/*!***********************!*\
  !*** ./pages/test.js ***!
  \***********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var qrcode__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! qrcode */ \"qrcode\");\n/* harmony import */ var qrcode__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(qrcode__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! next/router */ \"next/router\");\n/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_3__);\n\n\n\n\nconst Shop = ()=>{\n    const router = (0,next_router__WEBPACK_IMPORTED_MODULE_3__.useRouter)();\n    const onButtonClick = (0,react__WEBPACK_IMPORTED_MODULE_2__.useCallback)(()=>{\n        router.push(\"/test2\");\n    }, [\n        router\n    ]);\n    const { 0: url , 1: setUrl  } = (0,react__WEBPACK_IMPORTED_MODULE_2__.useState)(\"\");\n    const { 0: qr , 1: setQr  } = (0,react__WEBPACK_IMPORTED_MODULE_2__.useState)(\"\");\n    const GenerateQRCode = ()=>{\n        qrcode__WEBPACK_IMPORTED_MODULE_1___default().toDataURL(url, {\n            width: 800,\n            margin: 2,\n            color: {\n                dark: \"#335383FF\",\n                light: \"#EEEEEEFF\"\n            }\n        }, (err, url)=>{\n            if (err) return console.error(err);\n            console.log(url);\n            setQr(url);\n        });\n    };\n    return /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"div\", {\n        className: \"app\",\n        children: [\n            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"h1\", {\n                children: \"QR Generator\"\n            }, void 0, false, {\n                fileName: \"H:\\\\shareAshelf\\\\pages\\\\test.js\",\n                lineNumber: 33,\n                columnNumber: 4\n            }, undefined),\n            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"input\", {\n                type: \"text\",\n                placeholder: \"e.g. https://google.com\",\n                value: url,\n                onChange: (e)=>setUrl(e.target.value)\n            }, void 0, false, {\n                fileName: \"H:\\\\shareAshelf\\\\pages\\\\test.js\",\n                lineNumber: 34,\n                columnNumber: 4\n            }, undefined),\n            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"div\", {\n                onClick: onButtonClick,\n                children: /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"button\", {\n                    onClick: GenerateQRCode,\n                    children: \"Generate\"\n                }, void 0, false, {\n                    fileName: \"H:\\\\shareAshelf\\\\pages\\\\test.js\",\n                    lineNumber: 40,\n                    columnNumber: 4\n                }, undefined)\n            }, void 0, false, {\n                fileName: \"H:\\\\shareAshelf\\\\pages\\\\test.js\",\n                lineNumber: 39,\n                columnNumber: 17\n            }, undefined)\n        ]\n    }, void 0, true, {\n        fileName: \"H:\\\\shareAshelf\\\\pages\\\\test.js\",\n        lineNumber: 32,\n        columnNumber: 3\n    }, undefined);\n};\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Shop);\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9wYWdlcy90ZXN0LmpzLmpzIiwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7OztBQUFBO0FBQTRCO0FBQ21CO0FBQ1A7QUFFeEMsTUFBTUksSUFBSSxHQUFHLElBQU87SUFDaEIsTUFBTUMsTUFBTSxHQUFHRixzREFBUyxFQUFFO0lBRTFCLE1BQU1HLGFBQWEsR0FBR0osa0RBQVcsQ0FBQyxJQUFNO1FBQ3BDRyxNQUFNLENBQUNFLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUN4QixDQUFDLEVBQUU7UUFBQ0YsTUFBTTtLQUFDLENBQUM7SUFFakIsTUFBTSxLQUFDRyxHQUFHLE1BQUVDLE1BQU0sTUFBSVIsK0NBQVEsQ0FBQyxFQUFFLENBQUM7SUFDbEMsTUFBTSxLQUFDUyxFQUFFLE1BQUVDLEtBQUssTUFBSVYsK0NBQVEsQ0FBQyxFQUFFLENBQUM7SUFFaEMsTUFBTVcsY0FBYyxHQUFHLElBQU07UUFDNUJaLHVEQUFnQixDQUFDUSxHQUFHLEVBQUU7WUFDckJNLEtBQUssRUFBRSxHQUFHO1lBQ1ZDLE1BQU0sRUFBRSxDQUFDO1lBQ1RDLEtBQUssRUFBRTtnQkFDTkMsSUFBSSxFQUFFLFdBQVc7Z0JBQ2pCQyxLQUFLLEVBQUUsV0FBVzthQUNsQjtTQUNELEVBQUUsQ0FBQ0MsR0FBRyxFQUFFWCxHQUFHLEdBQUs7WUFDaEIsSUFBSVcsR0FBRyxFQUFFLE9BQU9DLE9BQU8sQ0FBQ0MsS0FBSyxDQUFDRixHQUFHLENBQUM7WUFFbENDLE9BQU8sQ0FBQ0UsR0FBRyxDQUFDZCxHQUFHLENBQUM7WUFDaEJHLEtBQUssQ0FBQ0gsR0FBRyxDQUFDO1FBQ1gsQ0FBQyxDQUFDO0lBQ0gsQ0FBQztJQUVELHFCQUNDLDhEQUFDZSxLQUFHO1FBQUNDLFNBQVMsRUFBQyxLQUFLOzswQkFDbkIsOERBQUNDLElBQUU7MEJBQUMsY0FBWTs7Ozs7eUJBQUs7MEJBQ3JCLDhEQUFDQyxPQUFLO2dCQUNMQyxJQUFJLEVBQUMsTUFBTTtnQkFDWEMsV0FBVyxFQUFDLHlCQUF5QjtnQkFDckNDLEtBQUssRUFBRXJCLEdBQUc7Z0JBQ1ZzQixRQUFRLEVBQUVDLENBQUFBLENBQUMsR0FBSXRCLE1BQU0sQ0FBQ3NCLENBQUMsQ0FBQ0MsTUFBTSxDQUFDSCxLQUFLLENBQUM7Ozs7O3lCQUFJOzBCQUM3Qiw4REFBQ04sS0FBRztnQkFBQ1UsT0FBTyxFQUFFM0IsYUFBYTswQkFDeEMsNEVBQUM0QixRQUFNO29CQUFDRCxPQUFPLEVBQUVyQixjQUFjOzhCQUFFLFVBQVE7Ozs7OzZCQUFTOzs7Ozt5QkFDbkM7Ozs7OztpQkFFVixDQUNOO0FBQ0YsQ0FBQztBQUVELGlFQUFlUixJQUFJLEVBQUMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9zaGFyYXNoZWxmLy4vcGFnZXMvdGVzdC5qcz9kMDk5Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBRUkNvZGUgZnJvbSAncXJjb2RlJztcclxuaW1wb3J0IHsgdXNlU3RhdGUgLCB1c2VDYWxsYmFjayB9IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IHsgdXNlUm91dGVyIH0gZnJvbSBcIm5leHQvcm91dGVyXCI7XHJcblxyXG5jb25zdCBTaG9wID0gKCkgPT4gIHtcclxuICAgIGNvbnN0IHJvdXRlciA9IHVzZVJvdXRlcigpO1xyXG5cclxuICAgIGNvbnN0IG9uQnV0dG9uQ2xpY2sgPSB1c2VDYWxsYmFjaygoKSA9PiB7XHJcbiAgICAgICAgcm91dGVyLnB1c2goXCIvdGVzdDJcIik7XHJcbiAgICAgIH0sIFtyb3V0ZXJdKTtcclxuXHJcblx0Y29uc3QgW3VybCwgc2V0VXJsXSA9IHVzZVN0YXRlKCcnKVxyXG5cdGNvbnN0IFtxciwgc2V0UXJdID0gdXNlU3RhdGUoJycpXHJcblxyXG5cdGNvbnN0IEdlbmVyYXRlUVJDb2RlID0gKCkgPT4ge1xyXG5cdFx0UVJDb2RlLnRvRGF0YVVSTCh1cmwsIHtcclxuXHRcdFx0d2lkdGg6IDgwMCxcclxuXHRcdFx0bWFyZ2luOiAyLFxyXG5cdFx0XHRjb2xvcjoge1xyXG5cdFx0XHRcdGRhcms6ICcjMzM1MzgzRkYnLFxyXG5cdFx0XHRcdGxpZ2h0OiAnI0VFRUVFRUZGJ1xyXG5cdFx0XHR9XHJcblx0XHR9LCAoZXJyLCB1cmwpID0+IHtcclxuXHRcdFx0aWYgKGVycikgcmV0dXJuIGNvbnNvbGUuZXJyb3IoZXJyKVxyXG5cclxuXHRcdFx0Y29uc29sZS5sb2codXJsKVxyXG5cdFx0XHRzZXRRcih1cmwpXHJcblx0XHR9KVxyXG5cdH1cclxuXHJcblx0cmV0dXJuIChcclxuXHRcdDxkaXYgY2xhc3NOYW1lPVwiYXBwXCI+XHJcblx0XHRcdDxoMT5RUiBHZW5lcmF0b3I8L2gxPlxyXG5cdFx0XHQ8aW5wdXQgXHJcblx0XHRcdFx0dHlwZT1cInRleHRcIlxyXG5cdFx0XHRcdHBsYWNlaG9sZGVyPVwiZS5nLiBodHRwczovL2dvb2dsZS5jb21cIlxyXG5cdFx0XHRcdHZhbHVlPXt1cmx9XHJcblx0XHRcdFx0b25DaGFuZ2U9e2UgPT4gc2V0VXJsKGUudGFyZ2V0LnZhbHVlKX0gLz5cclxuICAgICAgICAgICAgICAgIDxkaXYgb25DbGljaz17b25CdXR0b25DbGlja30gPlxyXG5cdFx0XHQ8YnV0dG9uIG9uQ2xpY2s9e0dlbmVyYXRlUVJDb2RlfT5HZW5lcmF0ZTwvYnV0dG9uPlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuXHRcdFx0XHJcblx0XHQ8L2Rpdj5cclxuXHQpXHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IFNob3A7Il0sIm5hbWVzIjpbIlFSQ29kZSIsInVzZVN0YXRlIiwidXNlQ2FsbGJhY2siLCJ1c2VSb3V0ZXIiLCJTaG9wIiwicm91dGVyIiwib25CdXR0b25DbGljayIsInB1c2giLCJ1cmwiLCJzZXRVcmwiLCJxciIsInNldFFyIiwiR2VuZXJhdGVRUkNvZGUiLCJ0b0RhdGFVUkwiLCJ3aWR0aCIsIm1hcmdpbiIsImNvbG9yIiwiZGFyayIsImxpZ2h0IiwiZXJyIiwiY29uc29sZSIsImVycm9yIiwibG9nIiwiZGl2IiwiY2xhc3NOYW1lIiwiaDEiLCJpbnB1dCIsInR5cGUiLCJwbGFjZWhvbGRlciIsInZhbHVlIiwib25DaGFuZ2UiLCJlIiwidGFyZ2V0Iiwib25DbGljayIsImJ1dHRvbiJdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./pages/test.js\n");

/***/ }),

/***/ "./pages/test2.js":
/*!************************!*\
  !*** ./pages/test2.js ***!
  \************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _test__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./test */ \"./pages/test.js\");\n\n\nconst Shope = ()=>{\n    return /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"div\", {\n        className: \"app\",\n        children: [\n            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"h1\", {\n                children: \"QR Generator\"\n            }, void 0, false, {\n                fileName: \"H:\\\\shareAshelf\\\\pages\\\\test2.js\",\n                lineNumber: 7,\n                columnNumber: 4\n            }, undefined),\n            _test__WEBPACK_IMPORTED_MODULE_1__[\"default\"] && /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {\n                children: [\n                    /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"img\", {\n                        src: _test__WEBPACK_IMPORTED_MODULE_1__[\"default\"]\n                    }, void 0, false, {\n                        fileName: \"H:\\\\shareAshelf\\\\pages\\\\test2.js\",\n                        lineNumber: 10,\n                        columnNumber: 5\n                    }, undefined),\n                    /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"a\", {\n                        href: _test__WEBPACK_IMPORTED_MODULE_1__[\"default\"],\n                        download: \"qrcode.png\",\n                        children: \"Download\"\n                    }, void 0, false, {\n                        fileName: \"H:\\\\shareAshelf\\\\pages\\\\test2.js\",\n                        lineNumber: 11,\n                        columnNumber: 5\n                    }, undefined)\n                ]\n            }, void 0, true)\n        ]\n    }, void 0, true, {\n        fileName: \"H:\\\\shareAshelf\\\\pages\\\\test2.js\",\n        lineNumber: 6,\n        columnNumber: 3\n    }, undefined);\n};\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Shope);\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9wYWdlcy90ZXN0Mi5qcy5qcyIsIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7QUFBd0I7QUFFeEIsTUFBTUMsS0FBSyxHQUFHLElBQU87SUFFcEIscUJBQ0MsOERBQUNDLEtBQUc7UUFBQ0MsU0FBUyxFQUFDLEtBQUs7OzBCQUNuQiw4REFBQ0MsSUFBRTswQkFBQyxjQUFZOzs7Ozt5QkFBSztZQUVwQkosNkNBQUUsa0JBQUk7O2tDQUNOLDhEQUFDSyxLQUFHO3dCQUFDQyxHQUFHLEVBQUVOLDZDQUFFOzs7OztpQ0FBSTtrQ0FDaEIsOERBQUNPLEdBQUM7d0JBQUNDLElBQUksRUFBRVIsNkNBQUU7d0JBQUVTLFFBQVEsRUFBQyxZQUFZO2tDQUFDLFVBQVE7Ozs7O2lDQUFJOzs0QkFDN0M7Ozs7OztpQkFDRSxDQUNOO0FBQ0YsQ0FBQztBQUVELGlFQUFlUixLQUFLLEVBQUMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9zaGFyYXNoZWxmLy4vcGFnZXMvdGVzdDIuanM/YWVlMyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgcXIgZnJvbSBcIi4vdGVzdFwiO1xyXG5cclxuY29uc3QgU2hvcGUgPSAoKSA9PiAge1xyXG4gIFxyXG5cdHJldHVybiAoXHJcblx0XHQ8ZGl2IGNsYXNzTmFtZT1cImFwcFwiPlxyXG5cdFx0XHQ8aDE+UVIgR2VuZXJhdG9yPC9oMT5cclxuXHRcdFx0XHJcblx0XHRcdHtxciAmJiA8PlxyXG5cdFx0XHRcdDxpbWcgc3JjPXtxcn0gLz5cclxuXHRcdFx0XHQ8YSBocmVmPXtxcn0gZG93bmxvYWQ9XCJxcmNvZGUucG5nXCI+RG93bmxvYWQ8L2E+XHJcblx0XHRcdDwvPn1cclxuXHRcdDwvZGl2PlxyXG5cdClcclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgU2hvcGU7Il0sIm5hbWVzIjpbInFyIiwiU2hvcGUiLCJkaXYiLCJjbGFzc05hbWUiLCJoMSIsImltZyIsInNyYyIsImEiLCJocmVmIiwiZG93bmxvYWQiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./pages/test2.js\n");

/***/ }),

/***/ "next/router":
/*!******************************!*\
  !*** external "next/router" ***!
  \******************************/
/***/ ((module) => {

module.exports = require("next/router");

/***/ }),

/***/ "qrcode":
/*!*************************!*\
  !*** external "qrcode" ***!
  \*************************/
/***/ ((module) => {

module.exports = require("qrcode");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/***/ ((module) => {

module.exports = require("react");

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/***/ ((module) => {

module.exports = require("react/jsx-dev-runtime");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__("./pages/test2.js"));
module.exports = __webpack_exports__;

})();